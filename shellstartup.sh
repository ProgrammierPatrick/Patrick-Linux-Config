export VISUAL="vim"

alias ls='ls --color=auto'
alias grep='grep --color=auto'

alias la='ls -a'
alias ll='ls -hl'
alias lla='ls -ahl'

alias gits='git status'
alias gita='git add'
alias gitc='git commit'
alias gitd='git diff'
alias gitl='git log'
alias gitpl='git pull'
alias gitps='git push'
alias gitch='git checkout'

alias makecm='cmake .. && make'
alias makecmt='cmake .. && make && make test'
alias makemt='make && make test'

alias makedm='cmake -DCMAKE_BUILD_TYPE=Debug .. && make'
alias makedmt='cmake -DCMAKE_BUILD_TYPE=Debug .. && make && make test'

alias playcd='mplayer -cdrom-device /dev/cdrom cdda://'

if [ -n "$BASH_VERSION" ]; then
	# if in BASH

	PS1='$(x=$?; if [[ $x != 0 ]]; then printf "\[\033[0;31m\]%s " $x; fi)\[\033[1;32m\]\u\[\033[0;33m\]@\h \[\033[1;36m\]\w \[\033[1;32m\]\$ \[\033[0m\]'

	if command -v beet >/dev/null 2>&1 ; then
		source /share/system/Patrick-Linux-Config/beetcompletion
	fi
fi

# save current loation after each command
PROMPT_COMMAND='if [[ $(pwd) != "$HOME" ]] ; then pwd > /tmp/whereami ; fi'
