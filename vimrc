set number
set mouse=a
syntax on
set hlsearch "search highlight

" do not skip parts of line when a line is wrapped into multipe lines
noremap k gk
noremap j gj


" ======  tabs and spaces  ===

" tab size
set tabstop=4
set shiftwidth=4

" show tabs and spaces as colors (green: spaces, blue: tabs)


highlight LeadingTab ctermbg=blue guibg=blue
highlight LeadingSpace ctermbg=darkgreen guibg=darkgreen
highlight EvilSpace ctermbg=darkred guibg=darkred
function! ShowIndent()
    syn match LeadingTab /^\t\+/
    syn match LeadingSpace /^\ \+/
    syn match EvilSpace /\(^\t*\)\@<!\t\+/ " tabs not preceeded by tabs
    syn match EvilSpace /[ \t]\+$/ " trailing space
endfunction
command ShowIndent call ShowIndent()

" set expand tab to match file (default: ascii tab)
autocmd BufRead * :let &l:expandtab =
    \ getline(search('\v^%(\t| )', 'wn'))[0] ==# ' '
"     \-----read line------------------/ \first char is space/
"             \-get idx of '\t| ' line-/
"
