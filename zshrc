# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/patrick/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

#plugins=(
#	common-aliases
#	dircycle
#	dirhistory
#	wd
#)

source /share/system/Patrick-Linux-Config/shellstartup.sh
source /share/system/Patrick-Linux-Config/liquidprompt/liquidprompt
